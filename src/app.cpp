#include "app.hpp"

std::ostream& operator <<(std::ostream& os, const QueryResult&rhs) {
    os << rhs.img_path << "; "
        << rhs.posVec[0] << ',' << rhs.posVec[1] << ',' << rhs.posVec[2] << "; "
        << rhs.fwdVec[0] << ',' << rhs.fwdVec[1] << ',' << rhs.fwdVec[2] << "; "
        << rhs.upVec[0] << ',' << rhs.upVec[1] << ',' << rhs.upVec[2];
    return os;
}

vtkSmartPointer<vtkActor> readColmapPoints(const std::string& path) {
    auto points = vtkSmartPointer<vtkPoints>::New();
    auto colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
    colors->SetNumberOfComponents(3);
    colors->SetName("color");

    std::ifstream pointsf(path, ios::binary|ios::in);
    std::cerr << "file is open: " << pointsf.is_open() << "\n";

    int64_t n_points;
    pointsf.read(reinterpret_cast<char*>(&n_points), sizeof(int64_t));

    std::cerr << "n_points: " << n_points << "\n";

    for (auto i=0; i<n_points; i++) {
        uint64_t pt_id;
        pointsf.read(reinterpret_cast<char*>(&pt_id), sizeof(uint64_t));

        double xyz[3];
        pointsf.read(reinterpret_cast<char*>(xyz), sizeof(double)*3);

        uint8_t rgb[3];
        pointsf.read(reinterpret_cast<char*>(rgb), sizeof(uint8_t)*3);

        double error;
        pointsf.read(reinterpret_cast<char*>(&error), sizeof(double));

        int64_t track_length;
        pointsf.read(reinterpret_cast<char*>(&track_length), sizeof(int64_t));

        std::vector<std::array<int32_t, 2>>tracks;
        tracks.resize(track_length);
        pointsf.read(reinterpret_cast<char*>(tracks.data()), sizeof(int32_t)*2*track_length);

        points->InsertNextPoint(xyz);
        colors->InsertNextTypedTuple(rgb);

        if (i % 10000 == 0)
            std::cout << i << "/" << n_points << " read.\r" << std::flush;
    }
    std::cout << "\n";

    auto polydata = vtkSmartPointer<vtkPolyData>::New();
    polydata->SetPoints(points);
    polydata->GetPointData()->SetScalars(colors);

    auto vertexGlyphFilter = vtkSmartPointer<vtkVertexGlyphFilter>::New();
    vertexGlyphFilter->AddInputData(polydata);
    vertexGlyphFilter->Update();

    auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(vertexGlyphFilter->GetOutputPort());
    auto actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetPointSize(2);

    return actor;
}



std::vector<QueryResult> readQueryResults(const std::string& path) {

    std::vector<QueryResult> queries;
    std::ifstream posesf(path, ios::in);

    auto count = 0;

    std::cerr << "reading queries results...\n";
    while (!posesf.eof()) {
        if (++count%100 == 0) {
            std::cerr << count << " read...\r" << std::flush;
        }
        std::istringstream iss;
        std::string line;
        std::getline(posesf, line);
        iss.str(line);
        iss.clear();

        std::string img_name;
        std::array<double, 4> arr; // quaternion
        vtkQuaterniond quat, trans;
        try {
            iss >> img_name;
            for (auto& a: arr) iss >> a;
            quat.Set(arr.data());
            arr[0] = 0.;
            for (auto i=1; i<4; i++) iss >> arr[i];
            trans.Set(arr.data());
        } catch (...) {
            std::cerr << "failed at line " << count << '\n';
            continue;
        }

        // Too slow not doing it here...
        // // search for image file
        // glob->Reset();
        // glob->AddFileNames(img_name.c_str());
        // if (glob->GetNumberOfFileNames() == 0) {
        //     std::cerr << img_name << " does not exists...";
        //     continue;
        // }

        // Rotation and Traslation are world to cam coord, so need to invert
        // R' = R.t()
        quat.Normalize();
        quat.Invert();
        // T' = -R' * T
        trans = quat * trans * quat.Inverse();
        trans.Conjugate(); // invert vec excluding norm

        // then find the camera up (y) and forward(z) direction
        vtkQuaterniond y_quat, z_quat;
        y_quat.Set(0., 0., -1., 0.);
        z_quat.Set(0., 0., 0., 1.);
        y_quat = quat * y_quat * quat.Inverse();
        z_quat = quat * z_quat * quat.Inverse();

        queries.emplace_back();
        auto& query = queries.back();
        query.img_path = img_name;
        query.posVec = {trans.GetX(), trans.GetY(), trans.GetZ()};
        z_quat.GetRotationAngleAndAxis(query.fwdVec.data());
        y_quat.GetRotationAngleAndAxis(query.upVec.data());
    }

    std::cerr << "\n" << queries.size() << " queries results read.\n";

    return queries;
}
