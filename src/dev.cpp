#include "app.hpp"


class MyInteractorStyle: public vtkInteractorStyleTrackballCamera {
public:
    static MyInteractorStyle* New();
    vtkTypeMacro(MyInteractorStyle, vtkInteractorStyleTrackballCamera);

    std::function<void()> forward = [](){ std::cerr << "Forward.\n"; };
    std::function<void()> backward = [](){ std::cerr << "Backward.\n"; };

    virtual void OnKeyPress() override {
        auto* rwi = this->Interactor;
        std::string key = rwi->GetKeySym();
        // std::cerr << "Key Pressed: " << key << ".\n";

        if (key == "Up" || key == "Right") {
            forward();
        } else if (key == "Down" || key == "Left") {
            backward();
        }
        vtkInteractorStyleTrackballCamera::OnKeyPress();
    }
};
vtkStandardNewMacro(MyInteractorStyle);


int main(const int argc, const char *argv[]) {
    std::cerr << "Hello.\n";
    struct {
        std::string points_path = "../data/points3D_copy.bin";
        std::string pose_path = "../data/aachen_day_poses_copy.txt";
        std::string img_path ="/mnt/data/lufi_workspace/201904_image_based_localization/data/hfnet/data/aachen/images_upright/query/";
    } args;
    auto namedColors = vtkSmartPointer<vtkNamedColors>::New();

    // Reading colmap points
    auto pointsActor = readColmapPoints(args.points_path);
    auto queries = readQueryResults(args.pose_path);

    // create renderer
    double leftViewport[4] = {0.0, 0.0, 0.5, 1.0};
    double rightViewport[4] = {0.5, 0.0, 1.0, 1.0};
    double imgViewport[4] = {0.3, 0.0, 0.5, 0.4};

    auto leftRenderer = vtkSmartPointer<vtkRenderer>::New();
    auto rightRenderer = vtkSmartPointer<vtkRenderer>::New();
    auto cameraActor = vtkSmartPointer<vtkCameraActor>::New();
    leftRenderer->SetViewport(leftViewport);
    rightRenderer->SetViewport(rightViewport);
    leftRenderer->SetBackground(namedColors->GetColor3d("Navy").GetData());
    rightRenderer->SetBackground(namedColors->GetColor3d("Snow").GetData());
    cameraActor->SetCamera(rightRenderer->GetActiveCamera());
    cameraActor->GetProperty()->SetColor(namedColors->GetColor3d("Azure").GetData());

    leftRenderer->AddActor(pointsActor);
    leftRenderer->AddActor(cameraActor);
    rightRenderer->AddActor(pointsActor);
    rightRenderer->InteractiveOff();


    auto jpegReader = vtkSmartPointer<vtkJPEGReader>::New();
    // auto imgMapper = vtkSmartPointer<vtkImageMapper>::New();
    // auto imgActor = vtkSmartPointer<vtkActor2D>::New();
    // imgMapper->SetInputData(jpegReader->GetOutput());
    // imgMapper->SetColorWindow(255);
    // imgMapper->SetColorLevel(127.5);
    // imgMapper->RenderToRectangleOn();
    // imgActor->SetMapper(imgMapper);
    auto imgActor = vtkSmartPointer<vtkImageActor>::New();
    imgActor->SetInputData(jpegReader->GetOutput());
    auto imgRenderer = vtkSmartPointer<vtkRenderer>::New();
    imgRenderer->SetLayer(1);
    imgRenderer->AddActor(imgActor);
    imgRenderer->SetViewport(imgViewport);
    imgRenderer->SetBackground(namedColors->GetColor3d("Azure").GetData());
    imgRenderer->SetBackgroundAlpha(0.1);
    imgRenderer->GetActiveCamera()->ParallelProjectionOn();
    imgRenderer->InteractiveOff();

    // create window and interactor
    auto window = vtkSmartPointer<vtkRenderWindow>::New();
    auto interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    window->SetSize(1600, 600);
    window->SetNumberOfLayers(2);
    window->AddRenderer(leftRenderer);
    window->AddRenderer(rightRenderer);
    window->AddRenderer(imgRenderer);
    interactor->SetRenderWindow(window);

    auto style = vtkSmartPointer<MyInteractorStyle>::New();
    interactor->SetInteractorStyle(style);

    auto glob = vtkSmartPointer<vtkGlobFileNames>::New();
    glob->RecurseOn();
    glob->SetDirectory(args.img_path.c_str());

    auto setCam = [&](size_t i) {
        const auto& query = queries[i];
        std::cout << i << query << '\n';

        // search for image file
        glob->Reset();
        glob->AddFileNames(query.img_path.c_str());
        if (glob->GetNumberOfFileNames() > 0) {
            std::cerr << glob->GetNthFileName(0) << '\n';
            jpegReader->SetFileName(glob->GetNthFileName(0));
            jpegReader->Update();
            imgRenderer->ResetCamera();
            imgRenderer->DrawOn();
        } else {
            std::cerr << query.img_path << " not found.\n";
            imgRenderer->DrawOff();
        }

        // camera position
        rightRenderer->GetActiveCamera()->SetPosition(query.posVec.data());
        leftRenderer->GetActiveCamera()->SetFocalPoint(query.posVec.data());

        std::array<double, 3> focalPtVec(query.posVec);

        // camera viewing direction
        const double p = 50;
        for (auto i=0; i<3; i++) focalPtVec[i] += query.fwdVec[i];
        rightRenderer->GetActiveCamera()->SetFocalPoint(focalPtVec.data());

        // camera up direction
        rightRenderer->GetActiveCamera()->SetViewUp(query.upVec.data());

        window->Render();

    };
    size_t ptr = 0;
    style->forward = [&ptr, &queries, &setCam] () {
        ptr = ptr>=queries.size()-1 ? 0 : ptr+1;
        setCam(ptr);
    };
    style->backward = [&ptr, &queries, &setCam] () {
        ptr = ptr<=0 ? queries.size()-1 : ptr-1;
        setCam(ptr);
    };


    setCam(ptr);
    window->Render();
    interactor->Start();

    std::cout << "Completed.\n";
    return 0;
}