cmake_minimum_required(VERSION 3.3 FATAL_ERROR)

project(HfNetVisualizer)

set (CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(VTK COMPONENTS
    vtkCommonColor
    vtkCommonCore
    vtkCommonSystem
    vtkCommonDataModel
    vtkIOCore
    vtkIOImage
    vtkFiltersSources
    vtkInteractionStyle
    vtkRenderingCore
    vtkRenderingFreeType
    vtkRenderingOpenGL2 QUIET)
if (NOT VTK_FOUND)
    message("VTK Not Found: ${VTK_NOT_FOUND_MESSAGE}")
    return ()
endif()

message (STATUS "VTK_VERSION: ${VTK_VERSION}")
include_directories("include")

add_executable(visualizer src/app.cpp src/dev.cpp )
target_link_libraries(visualizer PRIVATE ${VTK_LIBRARIES})
