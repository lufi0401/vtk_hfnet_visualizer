#pragma once
#ifndef _APP_HPP
#define _APP_HPP

#include <iostream>
#include <fstream>
#include <cstdint>
#include <string>
#include <vector>
#include <sstream>
#include <functional>

#include <vtkSmartPointer.h>
#include <vtkNamedColors.h>

#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkPolyDataMapper.h>

#include <vtkQuaternion.h>

#include <vtkProperty.h>
#include <vtkCameraActor.h>
#include <vtkCamera.h>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>

#include <vtkGlobFileNames.h>
#include <vtkJPEGReader.h>
#include <vtkImageActor.h>
#include <vtkImageMapper.h>
#include <vtkActor2D.h>

#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL2); // VTK was built with vtkRenderingOpenGL2
VTK_MODULE_INIT(vtkInteractionStyle);

struct QueryResult{
    std::string img_path;
    std::array<double, 3> posVec;
    std::array<double, 3> fwdVec;
    std::array<double, 3> upVec;
};

std::ostream& operator << (std::ostream& os, const QueryResult&rhs);

vtkSmartPointer<vtkActor> readColmapPoints(const std::string& path);
std::vector<QueryResult> readQueryResults(const std::string& path);

#endif